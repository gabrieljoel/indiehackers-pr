// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue'

import Buefy from 'buefy'
import 'bulmaswatch/darkly/bulmaswatch.min.css'
import 'bulma-helpers/css/bulma-helpers.min.css'


export default function (Vue, { router, head, isClient }) {

  head.script.push({
    src: 'https://plausible.io/js/plausible.js',
    body: true,
    async: true,
    defer: true,
    dataDomain: 'indiehackerspr.com',
  });

  // Set default layout as a global component
  Vue.use(Buefy)

  Vue.component('Layout', DefaultLayout)

}
